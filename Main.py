import random
#Variavéis globais
filename = "config.txt"   

l1 = []
l2 = []
l3 = []
l_4 = []
l_1 = []
l_2 = []
l_3 = []

#Função que realiza a leitura o arquivo de configuração
def armazena_info(filename):
    vetor_info = []
    arquivo = open(filename, "r")
    for linha in arquivo:
        linha = linha.strip()
        vetor_info.append(linha)
    arquivo.close()
    return vetor_info

#Funções que constroem a cache e memoria principal
def constroi_memoria_principal():
    vetor = armazena_info(filename)
    info_bloco = vetor[2]
    info_linha = int(vetor[0])
    tam_memoria_principal = int(info_bloco) * int(info_linha)
    bloco = []
    endereco = []
    conteudo = []
    list = []
    i=0
    k=0
    
    for j in range(0,tam_memoria_principal): 
        while(i < info_linha):
            bloco.append(k)
            i += 1
        if(i % info_linha == 0):
            k += 1
            i = 0

    for j in range(0,tam_memoria_principal):
        endereco.append(j)
        conteudo.append(sorteia_endereco(50))

        list.append(bloco)
        list.append(endereco)
        list.append(conteudo)

    return bloco, endereco, conteudo

def controi_cache():
    vetor = armazena_info(filename)
    info_linha_cache = int(vetor[0])
    #info_bloco_cache = int(vetor[2])
    linha = []
    bloco_cache = []
    endereco_cache = []
    conteudo_cache = []
    i = 0
    y = 0

    for z in range(4):
        carregador = sorteia_endereco(15)
        print("Carregador", carregador)
        aux = carregador
        for aux in range(aux+4):
            bloco_cache.append(l1[aux])
            endereco_cache.append(l2[aux])
            conteudo_cache.append(l3[aux])

    for l in range(16): 
        while(i < info_linha_cache):
            linha.append(y)
            i += 1
        if(i % info_linha_cache == 0):
            y += 1
            i = 0

    return linha, bloco_cache, endereco_cache, conteudo_cache

def sorteia_endereco(valor):
    resultado = []
    while len(resultado) != 4:
        r = random.randint(0, valor)
        if r not in resultado:
            resultado.append(r)
            return r

#Função busca endereço na cache   
def busca_endereco(valor):
    hit = 0
    i = -1
    while(valor != l_2[i]):
        i += 1
        if(valor == int(l_2[i])):
            print("Read {} -> HIT linha {}".format(valor, l_4[i]))
            hit += 1
       
        # else:
        #     print("Read {} -> MISS linha {}".format(valor, l_4[i]))
    return hit
    
#Funções que mostram cache e principal
def show_memoria_principal():
    print('Bloco-Endereço-Conteudo')
    for i in range(64):
        print('{0} - {1} - {2}'.format(l1[i], l2[i], l3[i]))

def show_cache():
    print('Linha-Bloco-Endereço-Conteudo')
    for i in range(16):
        print('{0} - {1} - {2} - {3}'.format(l_4[i], l_1[i], l_2[i], l_3[i]))

# def main():

l1, l2, l3 = constroi_memoria_principal()
l_4, l_1, l_2, l_3 = controi_cache()
show_memoria_principal()
show_cache()
#busca_endereco(19)

# if __name__ =="__main__":
#     main()